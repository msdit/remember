import { ThemeProvider } from 'next-themes'
import { Provider } from 'react-redux'
import store from 'data/store'
import '../src/styles/index.scss'

export const parameters = {
  actions: { argTypesRegex: '^on[A-Z].*' },
  backgrounds: { disable: true },
  controls: {},
}

export const globalTypes = {
  themes: {
    defaultValue: ['light', 'dark'],
  },
}

export const decorators = [
  (Story) => (
    <ThemeProvider
      storageKey="data-theme"
      enableSystem={true}
      attribute="class"
    >
      <Provider store={store}>
        <Story />
      </Provider>
    </ThemeProvider>
  ),
]
