const testCase = [
  {
    title: 'group 1',
    tasks: [
      { title: 'Task 11', isDone: false },
      { title: 'Task 12', isDone: true },
      { title: 'Task 13', isDone: false },
    ],
  },
  {
    title: 'group 2',
    tasks: [],
  },
]

function openNewGroupForm() {
  // Find new group button and click it
  cy.get('div[aria-label="new_group"]').click()
  // The new group form should be shown
  cy.get('input#group_name').should('be.visible')
}

function openAndCloseForm(closeAction) {
  openNewGroupForm()
  closeAction()
  // Should close form
  cy.get('input#group_name').should('not.exist')
}

function createGroup(idx, saveAction) {
  openNewGroupForm()
  const title = testCase[idx].title
  // Fill form by group title
  cy.get('input#group_name').type(title)
  saveAction()
  // Should close form and save the group
  cy.get('input#group_name').should('not.exist')
  cy.get(`div[data-title="${title}"]`).should('be.visible')
}

describe('Add some groups', () => {
  beforeEach(() => {
    cy.visit('/')
  })

  it('should open form and close by Esc and cancel', () => {
    // Press Esc to close form
    openAndCloseForm(() => cy.get('input#group_name').type('{esc}'))
    // Click cancel to close form
    openAndCloseForm(() => cy.get('p').contains('cancel').click())
  })

  it('should create group and tasks', () => {
    // Press Enter to add group
    createGroup(0, () => cy.get('input#group_name').type('{enter}'))
    // Press Enter to add group
    createGroup(1, () => cy.get('button').contains('Add').click())

    // Create Tasks
    testCase[0].tasks.forEach((task) => {
      cy.get(`div[data-title="${testCase[0].title}"]`)
        .find(`div[aria-label="new_task"]`)
        .click()
      cy.get('input#task_name').type(`${task.title}{enter}`)
      cy.get(`div[data-title="${task.title}"]`).should(
        'have.attr',
        'data-status',
        'pending'
      )
      cy.get(`div[data-title="${task.title}"]`)
        .find('svg[aria-label="delete_task"]')
        .should('be.exist')
    })

    // Change a task status to done
    const doneTask = testCase[0].tasks.find((el) => el.isDone)
    cy.get(`div[data-title="${doneTask.title}"]`).find('button').click()
    cy.get(`div[data-title="${doneTask.title}"]`).should(
      'have.attr',
      'data-status',
      'done'
    )
    // Should not include done and delete button
    cy.get(`div[data-title="${doneTask.title}"]`)
      .find('button')
      .should('not.exist')
    cy.get(`div[data-title="${doneTask.title}"]`)
      .find('svg[aria-label="delete_task"]')
      .should('not.exist')

    // Delete a task
    const pendingTask = testCase[0].tasks.find((el) => !el.isDone)
    cy.get(`div[data-title="${pendingTask.title}"]`)
      .find('svg[aria-label="delete_task"]')
      .click()
    cy.get('button#confirm_delete').click()
    // Should not include deleted task
    cy.get(`div[data-title="${pendingTask.title}"]`).should('not.exist')

    // Delete a group
    const groupTitle = testCase[0].title
    cy.get(`div[data-title="${groupTitle}"]`)
      .find('svg[aria-label="delete_group"]')
      .click()
    cy.get('button#confirm_delete').click()
    // Should not include deleted group and it's tasks
    cy.get(`div[data-title="${doneTask.title}"]`).should('not.exist')
    cy.get(`div[data-title="${groupTitle}"]`).should('not.exist')
  })
})
