/** @type {import('tailwindcss').Config} */
const { fontFamily } = require('tailwindcss/defaultTheme')

module.exports = {
  content: ['./src/**/*.tsx'],
  darkMode: 'class',
  theme: {
    extend: {
      fontFamily: {
        primary: ['var(--main-font)', ...fontFamily.sans],
        serif: ['var(--main-font)', ...fontFamily.serif],
      },
      colors: {
        primary: '#000',
        secondary: '#ffc453',
      },
    },
  },
  plugins: [],
}
