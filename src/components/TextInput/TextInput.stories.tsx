import { MagnifyingGlassIcon } from '@heroicons/react/24/solid'
import { ComponentStory, ComponentMeta } from '@storybook/react'
import { createRef } from 'react'

import TextInput from './TextInput'

const hideArg = {
  table: {
    disable: true,
  },
}

export default {
  title: 'Components/TextInput',
  component: TextInput,
} as ComponentMeta<typeof TextInput>

const Template: ComponentStory<typeof TextInput> = (args) => (
  <TextInput {...args} />
)

export const Base = Template.bind({})
Base.args = {
  placeholder: 'Placeholder',
}

const searchRef = createRef<HTMLInputElement>()
export const SearchField = Template.bind({})
SearchField.args = {
  ref: searchRef,
  placeholder: 'Search Card ...',
  iconComponent: MagnifyingGlassIcon,
  onClear: () => {
    const element = searchRef?.current
    if (element && element.value !== '') {
      element.value = ''
    }
  },
}
SearchField.argTypes = {
  ref: hideArg,
  iconComponent: hideArg,
}
