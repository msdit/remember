import React, {
  ForwardRefRenderFunction,
  InputHTMLAttributes,
  ForwardRefExoticComponent,
  createElement,
  forwardRef,
} from 'react'
import { XMarkIcon } from '@heroicons/react/24/solid'

interface ITextInputProps extends InputHTMLAttributes<Element> {
  className?: string
  iconComponent?: ForwardRefExoticComponent<React.SVGProps<SVGSVGElement>>
  onClear?: () => void
}

const TextInput: ForwardRefRenderFunction<HTMLInputElement, ITextInputProps> = (
  props,
  ref
) => {
  const { className = '', iconComponent, onClear, ...otherProps } = props

  const paddingLClass = iconComponent ? 'pl-10' : 'pl-4'
  const paddingRClass = onClear ? 'pr-10' : 'pr-4'
  const paddingXClass = `${paddingLClass} ${paddingRClass}`

  return (
    <div className={`relative ${className}`}>
      <input
        className={`appearance-none border border-gray-300 dark:border-gray-700 rounded w-full py-2 focus:outline-none focus:border-gray-700 focus:dark:border-gray-300 ${paddingXClass}`}
        type="text"
        ref={ref}
        {...otherProps}
      />
      {iconComponent &&
        createElement(iconComponent, {
          className: 'absolute left-2 top-1/2 -translate-y-1/2 w-5 h-5',
        })}
      {onClear && (
        <XMarkIcon
          className="absolute right-2 top-1/2 -translate-y-1/2 w-5 h-5 opacity-40 cursor-pointer"
          onClick={onClear}
        />
      )}
    </div>
  )
}

export default forwardRef<HTMLInputElement, ITextInputProps>(TextInput)
