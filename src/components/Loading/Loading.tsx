import { FC } from 'react'
import { selectLoading } from 'data/reducer/generalReducer'
import { useAppSelector } from 'data/store'

const Loading: FC = () => {
  const isLoading = useAppSelector(selectLoading)

  return isLoading ? (
    <div className="fixed top-0 right-0 left-0 bottom-0 bg-opacity-50 bg-black flex justify-center items-center text-lg font-bold text-white">
      Loading ...
    </div>
  ) : (
    <></>
  )
}

export default Loading
