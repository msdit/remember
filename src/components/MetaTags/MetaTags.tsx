import Head from 'next/head'
import { FC, PropsWithChildren } from 'react'

interface IMetaTagsProps extends PropsWithChildren {
  title?: string
  description?: string
}

const MetaTags: FC<IMetaTagsProps> = (props) => {
  const { title, description, children } = props
  return (
    <Head>
      <title>Remember To Do! {title ? `| ${title}` : ''}</title>
      {description && <meta name="description" content={description} />}
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <meta name="robots" content="noindex,nofollow" />
      <link rel="icon" href="/favicon.ico" />
      {children}
    </Head>
  )
}

export default MetaTags
