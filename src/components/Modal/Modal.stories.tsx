import { ComponentStory, ComponentMeta } from '@storybook/react'
import Button from 'components/Button/Button'
import { changeModal } from 'data/reducer/generalReducer'
import { useAppDispatch } from 'data/store'

import Modal from './Modal'

export default {
  title: 'Components/Modal',
  component: Modal,
  decorators: [
    (Story) => {
      const dispatch = useAppDispatch()
      return (
        <>
          <Button
            onClick={() =>
              dispatch(
                changeModal({
                  title: 'This is title',
                  subTitle:
                    'This is subTitle, This is subTitle, This is subTitle, This is subTitle, This is subTitle.',
                  buttons: [
                    {
                      id: 'btn_1',
                      label: 'Sure',
                      action: () => console.log('Sure'),
                    },
                    {
                      id: 'btn_2',
                      label: 'Cancel',
                      className: 'bg-red-600 hover:bg-red-700',
                      action: () => console.log('Cancel'),
                    },
                  ],
                })
              )
            }
          >
            Show Modal
          </Button>
          <Story />
        </>
      )
    },
  ],
} as ComponentMeta<typeof Modal>

const Template: ComponentStory<typeof Modal> = (args) => <Modal {...args} />

export const Base = Template.bind({})
