import { Dialog, Transition } from '@headlessui/react'
import { XMarkIcon } from '@heroicons/react/24/solid'
import Button from 'components/Button/Button'
import { FC, Fragment } from 'react'
import { changeModal, selectModal } from 'data/reducer/generalReducer'
import { useAppDispatch, useAppSelector } from 'data/store'

const Modal: FC = () => {
  const modalContent = useAppSelector(selectModal)
  const dispatch = useAppDispatch()

  function closeModal(buttonAction?: () => void) {
    buttonAction && buttonAction()
    dispatch(changeModal(null))
  }

  const isOpen = !!modalContent

  return (
    <Transition appear show={isOpen} as={Fragment}>
      <Dialog as="div" className="relative z-10" onClose={() => closeModal()}>
        <Transition.Child
          as={Fragment}
          enter="ease-out duration-300"
          enterFrom="opacity-0"
          enterTo="opacity-100"
          leave="ease-in duration-200"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <div className="fixed inset-0 bg-black bg-opacity-25" />
        </Transition.Child>

        <div className="fixed inset-0 overflow-y-auto">
          <div className="flex min-h-full items-center justify-center p-4 text-center">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 scale-95"
              enterTo="opacity-100 scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 scale-100"
              leaveTo="opacity-0 scale-95"
            >
              <Dialog.Panel className="relative w-full max-w-md transform overflow-hidden rounded-2xl bg-white dark:bg-slate-900 p-6 text-left align-middle shadow-xl transition-all">
                <XMarkIcon
                  className="absolute w-4 h-4 top-2 right-2 text-red-400 hover hover:text-red-600"
                  title="Remove group"
                  aria-label="delete_group"
                  onClick={() => closeModal()}
                />
                {modalContent?.title && (
                  <Dialog.Title
                    as="h3"
                    className="text-lg font-medium leading-6 text-gray-900 dark:text-gray-100"
                  >
                    {modalContent.title}
                  </Dialog.Title>
                )}
                {modalContent?.subTitle && (
                  <div className="mt-2">
                    <p className="text-sm text-gray-600 dark:text-gray-400">
                      {modalContent?.subTitle}
                    </p>
                  </div>
                )}

                <div className="mt-4">
                  {modalContent?.buttons.map((btn) => (
                    <Button
                      key={btn.id}
                      id={btn.id}
                      className={`mr-2 ${btn.className}`}
                      onClick={() => closeModal(btn.action)}
                    >
                      {btn.label}
                    </Button>
                  ))}
                </div>
              </Dialog.Panel>
            </Transition.Child>
          </div>
        </div>
      </Dialog>
    </Transition>
  )
}

export default Modal
