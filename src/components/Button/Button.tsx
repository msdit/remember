import { FC, ButtonHTMLAttributes } from 'react'

interface IButtonProps extends ButtonHTMLAttributes<Element> {
  className?: string
}

const Button: FC<IButtonProps> = (props) => {
  const { className = '', ...otherProps } = props
  return (
    <button
      type="button"
      className={`bg-green-600 ${
        props.disabled ? '' : 'hover'
      } hover:bg-green-700 disabled:bg-green-800 disabled:text-gray-300 text-white px-8 py-1 rounded-sm font-semibold ${className}`}
      {...otherProps}
    />
  )
}

export default Button
