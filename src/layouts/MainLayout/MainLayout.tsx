import { FC, PropsWithChildren } from 'react'
import { Header } from 'modules'
import { Nunito } from 'next/font/google'
import { Loading, MetaTags, Modal } from 'components'

const font = Nunito({
  weight: ['400', '700'],
  style: ['normal'],
  subsets: ['latin'],
})

const MainLayout: FC<PropsWithChildren> = ({ children }) => (
  <>
    <MetaTags>
      <style jsx global>
        {`
          :root {
            --main-font: ${font.style.fontFamily};
          }
        `}
      </style>
    </MetaTags>
    <div className={font.className}>
      <Header />
      <main>{children}</main>
      <Modal />
      <Loading />
    </div>
  </>
)

export default MainLayout
