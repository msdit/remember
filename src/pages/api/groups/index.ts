import { connectMongo } from 'data/dbConnect'
import Group from 'data/models/Group'
import { NextApiRequest, NextApiResponse } from 'next'

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const { method } = req
  connectMongo()

  try {
    if (method === 'GET') {
      const groups = await Group.find({})
      return res.status(200).json({ success: true, data: groups })
    } else if (method === 'POST') {
      await Group.create(req.body)
      const groups = await Group.find({})
      return res.status(200).json({ success: true, data: groups })
    }
    return res.status(404)
  } catch (error) {
    return res
      .status(500)
      .json({ success: false, message: 'Internal server error' })
  }
}
