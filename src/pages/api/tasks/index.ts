import { connectMongo } from 'data/dbConnect'
import Task from 'data/models/Task'
import { NextApiRequest, NextApiResponse } from 'next'

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const { method } = req
  connectMongo()

  try {
    if (method === 'GET') {
      const tasks = await Task.find({})
      return res.status(200).json({ success: true, data: tasks })
    } else if (method === 'POST') {
      await Task.create({ ...req.body, createdDate: new Date(), isDone: false })
      const tasks = await Task.find({})
      return res.status(200).json({ success: true, data: tasks })
    }
    return res.status(404)
  } catch (error) {
    return res
      .status(500)
      .json({ success: false, message: 'Internal server error. T' })
  }
}
