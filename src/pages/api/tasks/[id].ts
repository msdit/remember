import { connectMongo } from 'data/dbConnect'
import Task from 'data/models/Task'
import { NextApiRequest, NextApiResponse } from 'next'

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const { method } = req
  connectMongo()

  try {
    if (method === 'DELETE') {
      const { id } = req.query
      const itemCount = await Task.countDocuments({ _id: id, isDone: false })
      if (itemCount > 0) {
        await Task.deleteOne({ _id: id })
        const tasks = await Task.find({})
        return res.status(200).json({ success: true, data: tasks })
      }
    }
    if (method === 'PATCH') {
      const { id } = req.query
      const itemCount = await Task.countDocuments({ _id: id, isDone: false })
      if (itemCount > 0) {
        await Task.updateOne({ _id: id }, { isDone: true })
        const tasks = await Task.find({})
        return res.status(200).json({ success: true, data: tasks })
      }
    }
    return res.status(404)
  } catch (error) {
    return res
      .status(500)
      .json({ success: false, message: 'Internal server error' })
  }
}
