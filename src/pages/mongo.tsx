import { Button } from 'components'
import { connectMongo } from 'data/dbConnect'
import Group from 'data/models/Group'
import Task from 'data/models/Task'
import { changeWithDB } from 'data/reducer/generalReducer'
import {
  fetchData,
  updateGroupList,
  updateTaskList,
} from 'data/reducer/taskManagerReducer'
import { useAppDispatch } from 'data/store'
import { NextPage } from 'next'
import { useEffect } from 'react'
import { Groups } from 'Template'
import { IGroupType, ITaskType } from 'types'

interface IMongoPageProps {
  groups: IGroupType[]
  tasks: ITaskType[]
}

const MongoPage: NextPage<IMongoPageProps> = (props) => {
  const { groups, tasks } = props
  const dispatch = useAppDispatch()

  useEffect(() => {
    dispatch(updateGroupList(groups))
    dispatch(updateTaskList(tasks))
    dispatch(changeWithDB(true))
  }, [groups, tasks, dispatch])

  useEffect(() => {
    dispatch(fetchData())
  }, [dispatch])

  return (
    <>
      {/* eslint-disable-next-line @next/next/no-html-link-for-pages */}
      <a href="/">
        <Button className="fixed bottom-5 right-5 z-50">
          Try without mongoDB
        </Button>
      </a>
      <Groups />
    </>
  )
}

export default MongoPage

export async function getStaticProps() {
  connectMongo()
  const [groups, tasks]: any = await Promise.allSettled([
    Group.find({}),
    Task.find({}),
  ])

  return {
    props: {
      groups: JSON.parse(JSON.stringify(groups.value)),
      tasks: JSON.parse(JSON.stringify(tasks.value)),
    },
    revalidate: 60,
  }
}
