import { Button } from 'components'
import { NextPage } from 'next'
import Link from 'next/link'
import { Groups } from 'Template'

const HomePage: NextPage = () => (
  <>
    <Link href="/mongo/">
      <Button className="fixed bottom-5 right-5 z-50">Try with mongoDB</Button>
    </Link>
    <Groups />
  </>
)

export default HomePage
