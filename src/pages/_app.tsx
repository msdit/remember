import 'styles/index.scss'
import type { AppProps } from 'next/app'
import { ThemeProvider } from 'next-themes'
import { MainLayout } from 'layouts'
import { Provider } from 'react-redux'
import store from 'data/store'

export default function App({ Component, pageProps }: AppProps) {
  return (
    <ThemeProvider enableSystem={true} attribute="class">
      <Provider store={store}>
        <MainLayout>
          <Component {...pageProps} />
        </MainLayout>
      </Provider>
    </ThemeProvider>
  )
}
