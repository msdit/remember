import { model, models, Schema } from 'mongoose'

const TaskSchema: Schema = new Schema({
  title: {
    type: String,
    required: true,
  },
  createdDate: {
    type: Date,
    required: true,
  },
  isDone: {
    type: String,
    required: true,
  },
  groupId: {
    type: String,
    required: true,
  },
})

export default models.Task || model('Task', TaskSchema)
