import { model, models, Schema } from 'mongoose'

const GroupSchema: Schema = new Schema({
  title: {
    type: String,
    required: true,
  },
})

export default models.Group || model('Group', GroupSchema)
