import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { AppState } from 'data/store'

interface IModalContentType {
  title?: string
  subTitle?: string
  buttons: {
    id: string
    label: string
    className?: string
    action?: () => void
  }[]
}

export interface IGeneralType {
  modal: IModalContentType | null
  withDB: boolean
  loading: boolean
}

const initialState: IGeneralType = {
  modal: null,
  withDB: false,
  loading: false,
}

export const generalSlice = createSlice({
  name: 'general',
  initialState,
  reducers: {
    changeModal: (state, action: PayloadAction<IModalContentType | null>) => {
      state.modal = action.payload
    },
    changeWithDB: (state, action: PayloadAction<boolean>) => {
      state.withDB = action.payload
    },
    changeLoading: (state, action: PayloadAction<boolean>) => {
      state.loading = action.payload
    },
  },
})

export const { changeModal, changeWithDB, changeLoading } = generalSlice.actions

export const selectModal = (state: AppState) => state.general.modal
export const selectWithDB = (state: AppState) => state.general.withDB
export const selectLoading = (state: AppState) => state.general.loading

export default generalSlice.reducer
