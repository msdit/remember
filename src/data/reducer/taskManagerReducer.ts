import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import {
  completeTaskReq,
  createGroupReq,
  createTaskReq,
  deleteGroupReq,
  deleteTaskReq,
  getGroupReq,
  getTaskReq,
} from 'data/api'
import { AppState, AppThunk } from 'data/store'
import { IGroupType, ITaskType } from 'types'
import { changeLoading } from './generalReducer'

export interface ITaskManagerType {
  groupList: IGroupType[]
  taskList: ITaskType[]
  searchString: string
}

const initialState: ITaskManagerType = {
  groupList: [],
  taskList: [],
  searchString: '',
}

export const taskManagerSlice = createSlice({
  name: 'taskManager',
  initialState,
  reducers: {
    updateGroupList: (state, action: PayloadAction<IGroupType[]>) => {
      state.groupList = action.payload
    },
    updateTaskList: (state, action: PayloadAction<ITaskType[]>) => {
      state.taskList = action.payload.map((el: any) => ({
        ...el,
        isDone:
          typeof el.isDone === 'boolean' ? el.isDone : el.isDone === 'true',
      }))
    },
    searchTask: (state, action: PayloadAction<string>) => {
      state.searchString = action.payload
    },
  },
})

export const { updateGroupList, updateTaskList, searchTask } =
  taskManagerSlice.actions

export const addGroup =
  (title: string): AppThunk =>
  async (dispatch, getState) => {
    dispatch(changeLoading(true))
    const withDB = getState().general.withDB
    if (!withDB) {
      const createdDate = new Date()
      const _id = createdDate.getTime().toString()
      const newList = [...getState().taskManager.groupList, { _id, title }]
      await dispatch(updateGroupList(newList))
    } else {
      const response = await createGroupReq(title)
      const { data } = await response.json()
      if (data) {
        await dispatch(updateGroupList(data))
      }
    }
    dispatch(changeLoading(false))
  }

export const removeGroup =
  (groupId: string): AppThunk =>
  async (dispatch, getState) => {
    dispatch(changeLoading(true))
    const withDB = getState().general.withDB
    if (!withDB) {
      const newGroupList = getState().taskManager.groupList.filter(
        ({ _id }) => _id !== groupId
      )
      await dispatch(updateGroupList(newGroupList))
      const newTaskList = getState().taskManager.taskList.filter(
        ({ groupId: gId }) => gId !== groupId
      )
      await dispatch(updateTaskList(newTaskList))
    } else {
      const response = await deleteGroupReq(groupId)
      const { data } = await response.json()
      if (data) {
        await dispatch(updateGroupList(data))
      }
    }
    dispatch(changeLoading(false))
  }

export const addTask =
  (data: { groupId: string; title: string }): AppThunk =>
  async (dispatch, getState) => {
    dispatch(changeLoading(true))
    const withDB = getState().general.withDB
    const { groupId, title } = data
    const createdDate = new Date()
    if (!withDB) {
      const _id = createdDate.getTime().toString()
      const newList = [
        ...getState().taskManager.taskList,
        { _id, title, createdDate, isDone: false, groupId },
      ]
      await dispatch(updateTaskList(newList))
    } else {
      const response = await createTaskReq({
        title,
        groupId,
      })
      const { data } = await response.json()
      if (data) {
        await dispatch(updateTaskList(data))
      }
    }
    dispatch(changeLoading(false))
  }

export const removeTask =
  (taskId: string): AppThunk =>
  async (dispatch, getState) => {
    dispatch(changeLoading(true))
    const withDB = getState().general.withDB
    if (!withDB) {
      const newGroupList = getState().taskManager.taskList.filter(
        ({ _id }) => _id !== taskId
      )
      await dispatch(updateTaskList(newGroupList))
    } else {
      const response = await deleteTaskReq(taskId)
      const { data } = await response.json()
      if (data) {
        await dispatch(updateTaskList(data))
      }
    }
    dispatch(changeLoading(false))
  }

export const completeTask =
  (taskId: string): AppThunk =>
  async (dispatch, getState) => {
    dispatch(changeLoading(true))
    const withDB = getState().general.withDB
    if (!withDB) {
      const list: ITaskType[] = JSON.parse(
        JSON.stringify(getState().taskManager.taskList)
      )
      const taskIndex = list.findIndex(({ _id }) => _id === taskId)
      list[taskIndex].isDone = true
      await dispatch(updateTaskList(list))
    } else {
      const response = await completeTaskReq(taskId)
      const { data } = await response.json()
      if (data) {
        await dispatch(updateTaskList(data))
      }
    }
    dispatch(changeLoading(false))
  }

export const fetchData = (): AppThunk => async (dispatch, getState) => {
  const [groupsRes, tasksRes]: any = await Promise.allSettled([
    getGroupReq(),
    getTaskReq(),
  ])
  const [{ value: groups }, { value: tasks }]: any = await Promise.allSettled([
    groupsRes.value.json(),
    tasksRes.value.json(),
  ])
  await Promise.allSettled([
    dispatch(updateGroupList(groups.data)),
    dispatch(updateTaskList(tasks.data)),
  ])
}

export const selectSearchString = (state: AppState) =>
  state.taskManager.searchString
export const selectGroup = (state: AppState) => state.taskManager.groupList
export const selectTask = (state: AppState) => {
  const searchString = state.taskManager.searchString
  return searchString
    ? state.taskManager.taskList.filter(({ title }) =>
        title.toLowerCase().includes(searchString.toLowerCase())
      )
    : state.taskManager.taskList
}

export default taskManagerSlice.reducer
