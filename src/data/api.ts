function sendReq(url: string, method = 'GET', body?: object) {
  return fetch(url, {
    method,
    headers: {
      'Content-Type': 'application/json',
    },
    body: body ? JSON.stringify(body) : null,
  })
}

export function getGroupReq() {
  return sendReq('/api/groups')
}

export function getTaskReq() {
  return sendReq('/api/tasks')
}

export function createGroupReq(title: string) {
  return sendReq('/api/groups', 'POST', { title })
}

export function deleteGroupReq(id: string) {
  return sendReq(`/api/groups/${id}`, 'DELETE')
}

export function createTaskReq(data: { title: string; groupId: string }) {
  return sendReq('/api/tasks', 'POST', data)
}

export function deleteTaskReq(id: string) {
  return sendReq(`/api/tasks/${id}`, 'DELETE')
}

export function completeTaskReq(id: string) {
  return sendReq(`/api/tasks/${id}`, 'PATCH')
}
