export interface IGroupType {
  _id: string
  title: string
}

export interface ITaskType {
  _id: string
  title: string
  createdDate: Date
  isDone: boolean
  groupId: string
}
