import { AddItem, GroupItem } from 'modules'
import TaskCard from 'modules/TaskCard/TaskCard'
import { FC } from 'react'
import { changeModal } from 'data/reducer/generalReducer'
import {
  selectGroup,
  selectTask,
  addGroup,
  removeGroup,
  addTask,
  removeTask,
  completeTask,
} from 'data/reducer/taskManagerReducer'
import { useAppDispatch, useAppSelector } from 'data/store'

const Groups: FC = () => {
  const groupList = useAppSelector(selectGroup)
  const taskList = useAppSelector(selectTask)

  const dispatch = useAppDispatch()

  const getGroupTasks = (groupId: string) => {
    return taskList
      .filter((task) => task.groupId === groupId)
      .sort((a, b) => (a.isDone === b.isDone ? 0 : a.isDone ? 1 : -1))
  }

  const deleteConfirmation = (id: string, type: 'group' | 'task') => {
    const title = (type === 'group' ? groupList : taskList).find(
      (el) => el._id === id
    )?.title

    dispatch(
      changeModal({
        title: 'Caution,',
        subTitle: `You tried to delete the "${title}" ${type}. Are you sure about this action?`,
        buttons: [
          {
            id: 'confirm_delete',
            label: 'Sure',
            action: () =>
              dispatch(type === 'group' ? removeGroup(id) : removeTask(id)),
          },
          {
            id: 'cancel_delete',
            label: 'HELL, No',
            className: 'bg-red-600 hover:bg-red-700',
          },
        ],
      })
    )
  }

  return (
    <div className="flex flex-col md:flex-row flex-nowrap items-center md:items-start overflow-auto md:h-[calc(100vh-64px)] pt-8 px-3 md:px-6">
      {groupList.map((group) => (
        <GroupItem
          {...group}
          onDelete={() => deleteConfirmation(group._id, 'group')}
          key={group._id}
        >
          <div>
            {getGroupTasks(group._id).map((task) => (
              <TaskCard
                {...task}
                key={task._id}
                onDelete={() => deleteConfirmation(task._id, 'task')}
                onComplete={() => dispatch(completeTask(task._id))}
              />
            ))}
            <AddItem
              addItem={async (title) => {
                await dispatch(addTask({ groupId: group._id, title }))
              }}
              label="Task"
              className="w-full mt-2"
            />
          </div>
        </GroupItem>
      ))}
      <AddItem
        addItem={async (title) => {
          await dispatch(addGroup(title))
        }}
        label="Group"
        className="w-full md:w-64 card shrink-0 mb-20 md:mb-0"
      />
    </div>
  )
}

export default Groups
