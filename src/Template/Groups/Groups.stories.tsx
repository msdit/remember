import { ComponentStory, ComponentMeta } from '@storybook/react'

import Groups from './Groups'

export default {
  title: 'Templates/Groups',
  component: Groups,
  parameters: {
    layout: 'fullscreen',
  },
} as ComponentMeta<typeof Groups>

const Template: ComponentStory<typeof Groups> = (args) => <Groups {...args} />

export const Base = Template.bind({})
Base.args = {
  label: 'Group',
  className: 'w-64 card text-center shrink-0 m-6',
}
