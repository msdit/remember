import { ComponentStory, ComponentMeta } from '@storybook/react'

import TaskCard from './TaskCard'

export default {
  title: 'Modules/TaskCard',
  component: TaskCard,
  parameters: {
    layout: 'fullscreen',
  },
  argTypes: {
    isDone: {
      control: { type: 'boolean' },
    },
    argTypes: { createdDate: { control: 'date' } },
  },
} as ComponentMeta<typeof TaskCard>

const Template: ComponentStory<typeof TaskCard> = (args) => (
  <TaskCard {...args} />
)

export const Base = Template.bind({})
Base.args = {
  _id: '0',
  title: 'Group Title',
  createdDate: new Date(),
  isDone: false,
  groupId: '0',
}
