import { CheckBadgeIcon, XMarkIcon } from '@heroicons/react/24/solid'
import { Button } from 'components'
import { FC } from 'react'
import { ITaskType } from 'types'

interface TaskCard extends ITaskType {
  onDelete: () => void
  onComplete: () => void
}

const TaskCard: FC<TaskCard> = (props) => {
  const { title, createdDate, isDone, onDelete, onComplete } = props

  return (
    <div
      className={`relative card shadow-md my-4 border-t-4 ${
        isDone
          ? 'bg-green-200 dark:bg-green-700 border-t-green-500'
          : 'bg-slate-100 dark:bg-slate-700 border-t-yellow-500'
      }`}
      data-title={title}
      data-status={isDone ? 'done' : 'pending'}
    >
      {!isDone && (
        <XMarkIcon
          className="absolute w-4 h-4 top-2 right-2 text-red-400 hover hover:text-red-600"
          title="Remove task"
          aria-label="delete_task"
          onClick={onDelete}
        />
      )}
      <div className="w-full flex flex-row justify-center">
        {isDone && (
          <CheckBadgeIcon className="w-5 h-5 mr-1 mt-1 text-green-500" />
        )}
        <p className={`${!isDone ? 'font-bold' : ''} mb-2 flex-1`}>{title}</p>
      </div>

      <p className="text-right text-xs opacity-30">
        {new Date(createdDate).toLocaleString()}
      </p>
      {!isDone && (
        <Button
          className="w-full mt-2 flex flex-row justify-center items-center"
          onClick={onComplete}
        >
          Mark as done
        </Button>
      )}
    </div>
  )
}

export default TaskCard
