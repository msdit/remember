import { ChangeEventHandler, FC } from 'react'
import { MagnifyingGlassIcon } from '@heroicons/react/24/solid'
import { TextInput } from 'components'
import { useAppDispatch, useAppSelector } from 'data/store'
import { searchTask, selectSearchString } from 'data/reducer/taskManagerReducer'

const SearchField: FC = () => {
  const searchString = useAppSelector(selectSearchString)
  const dispatch = useAppDispatch()

  const onClear = () => {
    dispatch(searchTask(''))
  }

  const onChange: ChangeEventHandler<HTMLInputElement> = (event) => {
    dispatch(searchTask(event.target.value))
  }

  return (
    <TextInput
      id="search"
      placeholder="Search tasks ..."
      iconComponent={MagnifyingGlassIcon}
      value={searchString}
      onChange={onChange}
      onClear={onClear}
      autoComplete="off"
    />
  )
}

export default SearchField
