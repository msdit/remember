import { createElement, FC, useEffect, useState } from 'react'
import { useTheme } from 'next-themes'
import { MoonIcon, SunIcon } from '@heroicons/react/24/solid'

const ChangeThemeButton: FC = () => {
  const { systemTheme, theme, setTheme } = useTheme()
  const [isRendered, setIsRendered] = useState(false)
  useEffect(() => {
    setIsRendered(true)
  }, [])

  const currentTheme = theme === 'system' ? systemTheme : theme
  const isDark = currentTheme === 'dark'

  const toggleTheme = () => {
    setTheme(isDark ? 'light' : 'dark')
  }

  const className = 'w-6 h-6'

  return isRendered ? (
    createElement(isDark ? SunIcon : MoonIcon, {
      key: currentTheme,
      className,
      role: 'button',
      onClick: toggleTheme,
    })
  ) : (
    <div className={className} />
  )
}

export default ChangeThemeButton
