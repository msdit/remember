import { FC } from 'react'
import ChangeThemeButton from './ChangeThemeButton'
import SearchField from './SearchField'

const Header: FC = () => {
  return (
    <>
      <header className="fixed bg-white dark:bg-slate-800 ring-1 ring-slate-900/5 shadow-xl w-full z-50">
        <div className="h-16 container mx-auto px-4 flex items-center justify-between">
          <div className="after:content-['R!'] md:after:content-['Remember!'] font-bold text-3xl leading-none" />
          <div className="flex-1 mx-2 md:mx-4">
            <SearchField />
          </div>
          <ChangeThemeButton />
        </div>
      </header>
      <div className="h-16 w-full" />
    </>
  )
}

export default Header
