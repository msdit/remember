import { ComponentStory, ComponentMeta } from '@storybook/react'

import AddItem from './AddItem'

export default {
  title: 'Modules/AddItem',
  component: AddItem,
  parameters: {
    layout: 'fullscreen',
  },
  argTypes: {
    addItem: {
      table: {
        disable: true,
      },
    },
    className: {
      table: {
        disable: true,
      },
    },
  },
} as ComponentMeta<typeof AddItem>

const Template: ComponentStory<typeof AddItem> = (args) => <AddItem {...args} />

export const NewGroup = Template.bind({})
NewGroup.args = {
  label: 'Group',
  className: 'w-64 card text-center shrink-0 m-6',
}

export const NewTask = Template.bind({})
NewTask.args = {
  label: 'Task',
  className: 'w-full mt-2',
}
