import { Button, TextInput } from 'components'
import { createRef, FC, KeyboardEventHandler, useEffect } from 'react'

interface IAddFormProps {
  onSubmit: (title: string) => void
  onCancel: () => void
  label: string
  loading?: boolean
}

const AddForm: FC<IAddFormProps> = (props) => {
  const { onSubmit, onCancel, label, loading } = props
  const inputRef = createRef<HTMLInputElement>()

  useEffect(() => {
    inputRef?.current?.focus()
  }, [inputRef])

  const submitForm = () => {
    const value = inputRef?.current?.value
    if (value) onSubmit(value)
  }

  const handleKeyPress: KeyboardEventHandler<HTMLInputElement> = (event) => {
    if (event.key === 'Enter') {
      submitForm()
    } else if (event.key === 'Escape') {
      onCancel()
    }
  }

  return (
    <>
      <TextInput
        disabled={loading}
        ref={inputRef}
        id={`${label.toLowerCase()}_name`}
        placeholder={`${label} Title`}
        onKeyDown={handleKeyPress}
      />
      <div className="flex items-center justify-end mt-2">
        {!loading && (
          <p
            className="underline underline-offset-2 cursor-pointer mr-4 text-sm text-blue-700 dark:text-blue-300"
            onClick={onCancel}
          >
            cancel
          </p>
        )}
        <Button disabled={loading} onClick={submitForm}>
          Add
        </Button>
      </div>
      <p className="text-xs opacity-30 mt-2">
        Press &ldquo;Enter&ldquo; to add and &ldquo;Esc&ldquo; to cancel
      </p>
    </>
  )
}

export default AddForm
