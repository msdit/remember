import { PlusIcon } from '@heroicons/react/24/solid'
import AddForm from './AddForm'
import { FC, useState } from 'react'

interface IAddItemProps {
  addItem: (title: string) => Promise<void>
  label: string
  className?: string
}

const AddItem: FC<IAddItemProps> = (props) => {
  const { addItem, label, className } = props
  const [isFormEnabled, setIsFormEnabled] = useState(false)
  const [loading, setLoading] = useState(false)

  const toggleForm = () => {
    setIsFormEnabled((prev) => !prev)
  }

  const onAddClick = (value: string) => {
    setLoading(true)
    addItem(value).finally(() => {
      toggleForm()
      setLoading(false)
    })
  }

  return (
    <div
      className={`${
        !isFormEnabled ? 'opacity-70 hover hover:opacity-100' : ''
      } ${className}`}
      aria-label={`new_${label.toLowerCase()}`}
      onClick={() => !isFormEnabled && toggleForm()}
    >
      {isFormEnabled ? (
        <AddForm
          loading={loading}
          onSubmit={onAddClick}
          onCancel={toggleForm}
          label={label}
        />
      ) : (
        <p className="m-0 font-bold flex justify-center items-center">
          <PlusIcon className="w-4 h-4 mr-1" />
          New {label}
        </p>
      )}
    </div>
  )
}

export default AddItem
