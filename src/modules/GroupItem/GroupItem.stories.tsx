import { ComponentStory, ComponentMeta } from '@storybook/react'

import GroupItem from './GroupItem'

export default {
  title: 'Modules/GroupItem',
  component: GroupItem,
  parameters: {
    layout: 'fullscreen',
  },
} as ComponentMeta<typeof GroupItem>

const Template: ComponentStory<typeof GroupItem> = (args) => (
  <GroupItem {...args} />
)

export const Base = Template.bind({})
Base.args = {
  title: 'Group Title',
  _id: '0',
}
