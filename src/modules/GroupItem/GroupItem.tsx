import { XMarkIcon } from '@heroicons/react/24/solid'
import { FC, PropsWithChildren } from 'react'
import { IGroupType } from 'types'

interface IGroupItemProps extends IGroupType, PropsWithChildren {
  onDelete: () => void
}

const GroupItem: FC<IGroupItemProps> = (props) => {
  const { title, onDelete, children } = props

  return (
    <div
      className="relative w-full md:w-64 card shrink-0 md:mr-8 mb-4 md:mb-10"
      data-title={title}
    >
      <XMarkIcon
        className="absolute w-4 h-4 top-2 right-2 text-red-400 hover hover:text-red-600"
        title="Remove group"
        aria-label="delete_group"
        onClick={onDelete}
      />
      <p className="font-bold text-xl truncate">{title}</p>
      {children}
    </div>
  )
}

export default GroupItem
